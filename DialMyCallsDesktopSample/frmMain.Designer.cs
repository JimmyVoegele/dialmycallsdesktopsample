﻿namespace DialMyCalls
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.lblReturnMessge = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.cmbDeleteContacts = new System.Windows.Forms.ComboBox();
            this.cmbGroups = new System.Windows.Forms.ComboBox();
            this.txtPhoneNumber = new Telerik.WinControls.UI.RadTextBox();
            this.txtLastName = new Telerik.WinControls.UI.RadTextBox();
            this.txtCreateGroupName = new Telerik.WinControls.UI.RadTextBox();
            this.txtEmail = new Telerik.WinControls.UI.RadTextBox();
            this.txtFirstName = new Telerik.WinControls.UI.RadTextBox();
            this.btnDeleteContact = new Telerik.WinControls.UI.RadButton();
            this.btnCreateGroup = new Telerik.WinControls.UI.RadButton();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.btnCreateContact = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadGrid1 = new Telerik.WinControls.UI.RadGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSendText = new Telerik.WinControls.UI.RadButton();
            this.txtMessage = new Telerik.WinControls.UI.RadTextBox();
            this.txtBroadcastName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.RadGrid2 = new Telerik.WinControls.UI.RadGridView();
            this.RadGrid3 = new Telerik.WinControls.UI.RadGridView();
            this.btnMakeCall = new Telerik.WinControls.UI.RadButton();
            this.txtCallBroadcastName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.cmbRecordings = new System.Windows.Forms.ComboBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.lblTextMessage = new System.Windows.Forms.Label();
            this.lblCalls = new System.Windows.Forms.Label();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnMessge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreateGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid1.MasterTemplate)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBroadcastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid3.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMakeCall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCallBroadcastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 57);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(780, 449);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.radLabel11);
            this.tabPage2.Controls.Add(this.radLabel10);
            this.tabPage2.Controls.Add(this.lblReturnMessge);
            this.tabPage2.Controls.Add(this.radLabel9);
            this.tabPage2.Controls.Add(this.cmbDeleteContacts);
            this.tabPage2.Controls.Add(this.cmbGroups);
            this.tabPage2.Controls.Add(this.txtPhoneNumber);
            this.tabPage2.Controls.Add(this.txtLastName);
            this.tabPage2.Controls.Add(this.txtCreateGroupName);
            this.tabPage2.Controls.Add(this.txtEmail);
            this.tabPage2.Controls.Add(this.txtFirstName);
            this.tabPage2.Controls.Add(this.btnDeleteContact);
            this.tabPage2.Controls.Add(this.btnCreateGroup);
            this.tabPage2.Controls.Add(this.radLabel8);
            this.tabPage2.Controls.Add(this.btnCreateContact);
            this.tabPage2.Controls.Add(this.radLabel4);
            this.tabPage2.Controls.Add(this.radLabel6);
            this.tabPage2.Controls.Add(this.radLabel5);
            this.tabPage2.Controls.Add(this.radLabel3);
            this.tabPage2.Controls.Add(this.radLabel7);
            this.tabPage2.Controls.Add(this.radLabel2);
            this.tabPage2.Controls.Add(this.RadGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(772, 416);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contacts";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel11.Location = new System.Drawing.Point(484, 211);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(2, 2);
            this.radLabel11.TabIndex = 16;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel10.Location = new System.Drawing.Point(407, 213);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(2, 2);
            this.radLabel10.TabIndex = 14;
            // 
            // lblReturnMessge
            // 
            this.lblReturnMessge.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblReturnMessge.Location = new System.Drawing.Point(407, 214);
            this.lblReturnMessge.Name = "lblReturnMessge";
            this.lblReturnMessge.Size = new System.Drawing.Size(2, 2);
            this.lblReturnMessge.TabIndex = 15;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel9.Location = new System.Drawing.Point(338, 214);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(63, 21);
            this.radLabel9.TabIndex = 13;
            this.radLabel9.Text = "Message:";
            // 
            // cmbDeleteContacts
            // 
            this.cmbDeleteContacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDeleteContacts.FormattingEnabled = true;
            this.cmbDeleteContacts.Location = new System.Drawing.Point(120, 382);
            this.cmbDeleteContacts.Name = "cmbDeleteContacts";
            this.cmbDeleteContacts.Size = new System.Drawing.Size(212, 24);
            this.cmbDeleteContacts.TabIndex = 8;
            // 
            // cmbGroups
            // 
            this.cmbGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGroups.FormattingEnabled = true;
            this.cmbGroups.Location = new System.Drawing.Point(120, 325);
            this.cmbGroups.Name = "cmbGroups";
            this.cmbGroups.Size = new System.Drawing.Size(212, 24);
            this.cmbGroups.TabIndex = 4;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhoneNumber.Location = new System.Drawing.Point(120, 269);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(212, 23);
            this.txtPhoneNumber.TabIndex = 2;
            // 
            // txtLastName
            // 
            this.txtLastName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLastName.Location = new System.Drawing.Point(120, 241);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(212, 23);
            this.txtLastName.TabIndex = 1;
            // 
            // txtCreateGroupName
            // 
            this.txtCreateGroupName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCreateGroupName.Location = new System.Drawing.Point(120, 354);
            this.txtCreateGroupName.Name = "txtCreateGroupName";
            this.txtCreateGroupName.Size = new System.Drawing.Size(212, 23);
            this.txtCreateGroupName.TabIndex = 6;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtEmail.Location = new System.Drawing.Point(120, 297);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(212, 23);
            this.txtEmail.TabIndex = 3;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFirstName.Location = new System.Drawing.Point(120, 213);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(212, 23);
            this.txtFirstName.TabIndex = 0;
            // 
            // btnDeleteContact
            // 
            this.btnDeleteContact.Location = new System.Drawing.Point(650, 382);
            this.btnDeleteContact.Name = "btnDeleteContact";
            this.btnDeleteContact.Size = new System.Drawing.Size(106, 24);
            this.btnDeleteContact.TabIndex = 9;
            this.btnDeleteContact.Text = "Delete Contact";
            this.btnDeleteContact.Click += new System.EventHandler(this.btnDeleteContact_Click);
            // 
            // btnCreateGroup
            // 
            this.btnCreateGroup.Location = new System.Drawing.Point(650, 353);
            this.btnCreateGroup.Name = "btnCreateGroup";
            this.btnCreateGroup.Size = new System.Drawing.Size(106, 24);
            this.btnCreateGroup.TabIndex = 7;
            this.btnCreateGroup.Text = "Create Group";
            this.btnCreateGroup.Click += new System.EventHandler(this.btnCreateGroup_Click);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel8.Location = new System.Drawing.Point(15, 384);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(60, 21);
            this.radLabel8.TabIndex = 6;
            this.radLabel8.Text = "Contacts";
            // 
            // btnCreateContact
            // 
            this.btnCreateContact.Location = new System.Drawing.Point(650, 325);
            this.btnCreateContact.Name = "btnCreateContact";
            this.btnCreateContact.Size = new System.Drawing.Size(106, 24);
            this.btnCreateContact.TabIndex = 5;
            this.btnCreateContact.Text = "Create Contact";
            this.btnCreateContact.Click += new System.EventHandler(this.btnCreateContact_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel4.Location = new System.Drawing.Point(15, 355);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(88, 21);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "Group Name:";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel6.Location = new System.Drawing.Point(15, 298);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(42, 21);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Email:";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel5.Location = new System.Drawing.Point(15, 270);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(102, 21);
            this.radLabel5.TabIndex = 5;
            this.radLabel5.Text = "Phone Number:";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel3.Location = new System.Drawing.Point(15, 242);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(74, 21);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Last Name:";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel7.Location = new System.Drawing.Point(15, 327);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(45, 21);
            this.radLabel7.TabIndex = 5;
            this.radLabel7.Text = "Group";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel2.Location = new System.Drawing.Point(14, 214);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(75, 21);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "First Name:";
            // 
            // RadGrid1
            // 
            this.RadGrid1.AutoSizeRows = true;
            this.RadGrid1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RadGrid1.Location = new System.Drawing.Point(6, 6);
            // 
            // 
            // 
            this.RadGrid1.MasterTemplate.AllowAddNewRow = false;
            this.RadGrid1.MasterTemplate.AllowColumnChooser = false;
            this.RadGrid1.MasterTemplate.AllowColumnReorder = false;
            this.RadGrid1.MasterTemplate.AllowColumnResize = false;
            this.RadGrid1.MasterTemplate.AllowDeleteRow = false;
            this.RadGrid1.MasterTemplate.AllowEditRow = false;
            this.RadGrid1.MasterTemplate.AllowRowResize = false;
            this.RadGrid1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.RadGrid1.MasterTemplate.ShowRowHeaderColumn = false;
            this.RadGrid1.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.RadGrid1.Name = "RadGrid1";
            this.RadGrid1.ShowGroupPanel = false;
            this.RadGrid1.Size = new System.Drawing.Size(766, 189);
            this.RadGrid1.TabIndex = 3;
            this.RadGrid1.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblTextMessage);
            this.tabPage1.Controls.Add(this.radLabel13);
            this.tabPage1.Controls.Add(this.radLabel12);
            this.tabPage1.Controls.Add(this.radLabel17);
            this.tabPage1.Controls.Add(this.txtBroadcastName);
            this.tabPage1.Controls.Add(this.radLabel18);
            this.tabPage1.Controls.Add(this.txtMessage);
            this.tabPage1.Controls.Add(this.radLabel19);
            this.tabPage1.Controls.Add(this.radLabel16);
            this.tabPage1.Controls.Add(this.RadGrid2);
            this.tabPage1.Controls.Add(this.btnSendText);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(772, 416);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Text Messsages";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSendText
            // 
            this.btnSendText.Location = new System.Drawing.Point(7, 386);
            this.btnSendText.Name = "btnSendText";
            this.btnSendText.Size = new System.Drawing.Size(110, 24);
            this.btnSendText.TabIndex = 10;
            this.btnSendText.Text = "Send Text";
            this.btnSendText.Click += new System.EventHandler(this.btnSendText_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMessage.Location = new System.Drawing.Point(132, 358);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(635, 23);
            this.txtMessage.TabIndex = 7;
            this.txtMessage.Text = "This is the text of the message.";
            // 
            // txtBroadcastName
            // 
            this.txtBroadcastName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBroadcastName.Location = new System.Drawing.Point(132, 330);
            this.txtBroadcastName.Name = "txtBroadcastName";
            this.txtBroadcastName.Size = new System.Drawing.Size(212, 23);
            this.txtBroadcastName.TabIndex = 6;
            this.txtBroadcastName.Text = "Broadcast Name";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel12.Location = new System.Drawing.Point(8, 359);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(89, 21);
            this.radLabel12.TabIndex = 9;
            this.radLabel12.Text = "Message Text";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel13.Location = new System.Drawing.Point(7, 331);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(110, 21);
            this.radLabel13.TabIndex = 8;
            this.radLabel13.Text = "Broadcast Name:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblCalls);
            this.tabPage3.Controls.Add(this.txtCallBroadcastName);
            this.tabPage3.Controls.Add(this.radLabel20);
            this.tabPage3.Controls.Add(this.cmbRecordings);
            this.tabPage3.Controls.Add(this.RadGrid3);
            this.tabPage3.Controls.Add(this.radLabel15);
            this.tabPage3.Controls.Add(this.radLabel14);
            this.tabPage3.Controls.Add(this.btnMakeCall);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(772, 416);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Calls";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.radLabel1.Location = new System.Drawing.Point(12, 22);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(387, 29);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.Text = "Dial My Calls Desktop Sample Application";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(662, 517);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 24);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Close";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // RadGrid2
            // 
            this.RadGrid2.AutoSizeRows = true;
            this.RadGrid2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RadGrid2.Location = new System.Drawing.Point(7, 6);
            // 
            // 
            // 
            this.RadGrid2.MasterTemplate.AllowAddNewRow = false;
            this.RadGrid2.MasterTemplate.AllowColumnChooser = false;
            this.RadGrid2.MasterTemplate.AllowColumnReorder = false;
            this.RadGrid2.MasterTemplate.AllowColumnResize = false;
            this.RadGrid2.MasterTemplate.AllowDeleteRow = false;
            this.RadGrid2.MasterTemplate.AllowRowResize = false;
            this.RadGrid2.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewCheckBoxColumn3.HeaderText = "Select";
            gridViewCheckBoxColumn3.Name = "chkSelect";
            gridViewCheckBoxColumn3.Width = 758;
            this.RadGrid2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn3});
            this.RadGrid2.MasterTemplate.ShowRowHeaderColumn = false;
            this.RadGrid2.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.RadGrid2.Name = "RadGrid2";
            this.RadGrid2.ShowGroupPanel = false;
            this.RadGrid2.Size = new System.Drawing.Size(759, 319);
            this.RadGrid2.TabIndex = 11;
            this.RadGrid2.TabStop = false;
            // 
            // RadGrid3
            // 
            this.RadGrid3.AutoSizeRows = true;
            this.RadGrid3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RadGrid3.Location = new System.Drawing.Point(6, 6);
            // 
            // 
            // 
            this.RadGrid3.MasterTemplate.AllowAddNewRow = false;
            this.RadGrid3.MasterTemplate.AllowColumnChooser = false;
            this.RadGrid3.MasterTemplate.AllowColumnReorder = false;
            this.RadGrid3.MasterTemplate.AllowColumnResize = false;
            this.RadGrid3.MasterTemplate.AllowDeleteRow = false;
            this.RadGrid3.MasterTemplate.AllowRowResize = false;
            this.RadGrid3.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewCheckBoxColumn4.HeaderText = "Select";
            gridViewCheckBoxColumn4.Name = "chkSelect";
            gridViewCheckBoxColumn4.Width = 758;
            this.RadGrid3.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn4});
            this.RadGrid3.MasterTemplate.ShowRowHeaderColumn = false;
            this.RadGrid3.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.RadGrid3.Name = "RadGrid3";
            this.RadGrid3.ShowGroupPanel = false;
            this.RadGrid3.Size = new System.Drawing.Size(759, 319);
            this.RadGrid3.TabIndex = 17;
            this.RadGrid3.TabStop = false;
            // 
            // btnMakeCall
            // 
            this.btnMakeCall.Location = new System.Drawing.Point(7, 385);
            this.btnMakeCall.Name = "btnMakeCall";
            this.btnMakeCall.Size = new System.Drawing.Size(110, 24);
            this.btnMakeCall.TabIndex = 16;
            this.btnMakeCall.Text = "Make Call";
            this.btnMakeCall.Click += new System.EventHandler(this.btnMakeCall_Click);
            // 
            // txtCallBroadcastName
            // 
            this.txtCallBroadcastName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCallBroadcastName.Location = new System.Drawing.Point(132, 330);
            this.txtCallBroadcastName.Name = "txtCallBroadcastName";
            this.txtCallBroadcastName.Size = new System.Drawing.Size(212, 23);
            this.txtCallBroadcastName.TabIndex = 12;
            this.txtCallBroadcastName.Text = "Broadcast Name";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel14.Location = new System.Drawing.Point(8, 359);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(75, 21);
            this.radLabel14.TabIndex = 15;
            this.radLabel14.Text = "Recordings";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel15.Location = new System.Drawing.Point(7, 331);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(72, 21);
            this.radLabel15.TabIndex = 14;
            this.radLabel15.Text = "Call Name:";
            // 
            // cmbRecordings
            // 
            this.cmbRecordings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRecordings.FormattingEnabled = true;
            this.cmbRecordings.Location = new System.Drawing.Point(132, 358);
            this.cmbRecordings.Name = "cmbRecordings";
            this.cmbRecordings.Size = new System.Drawing.Size(635, 24);
            this.cmbRecordings.TabIndex = 18;
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel16.Location = new System.Drawing.Point(277, 383);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(2, 2);
            this.radLabel16.TabIndex = 20;
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel17.Location = new System.Drawing.Point(200, 385);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(2, 2);
            this.radLabel17.TabIndex = 22;
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel18.Location = new System.Drawing.Point(200, 386);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(2, 2);
            this.radLabel18.TabIndex = 23;
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel19.Location = new System.Drawing.Point(131, 386);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(63, 21);
            this.radLabel19.TabIndex = 21;
            this.radLabel19.Text = "Message:";
            // 
            // lblTextMessage
            // 
            this.lblTextMessage.AutoSize = true;
            this.lblTextMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextMessage.Location = new System.Drawing.Point(200, 388);
            this.lblTextMessage.Name = "lblTextMessage";
            this.lblTextMessage.Size = new System.Drawing.Size(147, 17);
            this.lblTextMessage.TabIndex = 24;
            this.lblTextMessage.Text = "Text Result Messages";
            // 
            // lblCalls
            // 
            this.lblCalls.AutoSize = true;
            this.lblCalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalls.Location = new System.Drawing.Point(200, 387);
            this.lblCalls.Name = "lblCalls";
            this.lblCalls.Size = new System.Drawing.Size(143, 17);
            this.lblCalls.TabIndex = 26;
            this.lblCalls.Text = "Call Result Messages";
            // 
            // radLabel20
            // 
            this.radLabel20.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel20.Location = new System.Drawing.Point(131, 385);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(63, 21);
            this.radLabel20.TabIndex = 25;
            this.radLabel20.Text = "Message:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 551);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmMain";
            this.Text = "Dial My Calls Desktop Sample";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnMessge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreateGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBroadcastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMakeCall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCallBroadcastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGridView RadGrid1;
        private System.Windows.Forms.ComboBox cmbDeleteContacts;
        private System.Windows.Forms.ComboBox cmbGroups;
        private Telerik.WinControls.UI.RadTextBox txtPhoneNumber;
        private Telerik.WinControls.UI.RadTextBox txtLastName;
        private Telerik.WinControls.UI.RadTextBox txtCreateGroupName;
        private Telerik.WinControls.UI.RadTextBox txtEmail;
        private Telerik.WinControls.UI.RadTextBox txtFirstName;
        private Telerik.WinControls.UI.RadButton btnDeleteContact;
        private Telerik.WinControls.UI.RadButton btnCreateGroup;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadButton btnCreateContact;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel lblReturnMessge;
        private Telerik.WinControls.UI.RadButton btnSendText;
        private Telerik.WinControls.UI.RadTextBox txtMessage;
        private Telerik.WinControls.UI.RadTextBox txtBroadcastName;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadGridView RadGrid2;
        private Telerik.WinControls.UI.RadGridView RadGrid3;
        private System.Windows.Forms.ComboBox cmbRecordings;
        private Telerik.WinControls.UI.RadButton btnMakeCall;
        private Telerik.WinControls.UI.RadTextBox txtCallBroadcastName;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private System.Windows.Forms.Label lblTextMessage;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private System.Windows.Forms.Label lblCalls;
        private Telerik.WinControls.UI.RadLabel radLabel20;
    }
}

