﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

using IO.DialMyCalls.Model;
using Telerik.WinControls.UI;

namespace DialMyCalls
{
    public partial class frmMain : Form
    {
        DialMyCalls dmc = new DialMyCalls();

        public frmMain()
        {
            InitializeComponent();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ReloadAll();
        }

        private void btnCreateContact_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a First Name.";
                return;
            }

            if (txtLastName.Text.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a Last Name.";
                return;
            }

            string strPhoneNumber = txtPhoneNumber.Text.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
            if (strPhoneNumber.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a Phone Number.";
                return;
            }
            else
            {


                if (strPhoneNumber.Length != 10)
                {
                    lblReturnMessge.Text = "You have entered an invalid phone number.";
                    return;
                }
                long nPhoneNumber = 0;

                try
                {
                    nPhoneNumber = Convert.ToInt64(strPhoneNumber);
                }
                catch
                {
                    lblReturnMessge.Text = "There was an error converting your phone number to 10 digits.";
                    return;
                }

                if (txtEmail.Text.Length == 0)
                {
                    lblReturnMessge.Text = "You must enter an Email Address.";
                    return;
                }
                else
                {
                    if (!txtEmail.Text.Contains("@"))
                    {
                        lblReturnMessge.Text = "You must enter a valid Email Address.";
                        return;
                    }
                    if (!txtEmail.Text.Contains("."))
                    {
                        lblReturnMessge.Text = "You must enter a valid Email Address.";
                        return;
                    }

                }

                List<string> lst = new List<string>();
                KeyValuePair<string, string> kvp = (KeyValuePair < string, string > )cmbGroups.SelectedItem;


                lst.Add(kvp.Key);

                lblReturnMessge.Text = dmc.CreateContact(txtFirstName.Text, txtLastName.Text, strPhoneNumber, "", txtEmail.Text, lst);

                ReloadAll();
            }

        }

        private void btnCreateGroup_Click(object sender, EventArgs e)
        {
            dmc.CreateGroup(txtCreateGroupName.Text);
            ReloadAll();
        }



        private void btnDeleteContact_Click(object sender, EventArgs e)
        {
            if (cmbDeleteContacts.SelectedItem != null)
            {
                KeyValuePair<string, string> kvp = (KeyValuePair<string, string>)cmbDeleteContacts.SelectedItem;

                dmc.DeleteContact(kvp.Key);
                ReloadAll();
            }
        }



        private void btnSendText_Click(object sender, EventArgs e)
        {
            List<ContactAttributes> lstCA = new List<ContactAttributes>();

            foreach (GridViewRowInfo gRow in RadGrid2.Rows)
            {
                string bState =  "0";

                if (gRow.Cells[0].Value != null)
                {
                    bState = gRow.Cells[0].Value.ToString();
                }

                if (bState == "True")
                {
                    string FirstName = gRow.Cells[1].Value.ToString();
                    string LastName = gRow.Cells[2].Value.ToString();
                    string Phone = gRow.Cells[3].Value.ToString();
                    string Email = gRow.Cells[4].Value.ToString();
                    ContactAttributes CA = new ContactAttributes(Phone, FirstName, LastName, Email);
                    lstCA.Add(CA);
                }
            }
            if (lstCA.Count > 0)
            {
                try
                {
                    dmc.SendText(txtBroadcastName.Text, txtMessage.Text, lstCA);
                    lblTextMessage.Text = "Text sent successfully";
                }
                catch (Exception ex)
                {
                    lblTextMessage.Text = "Text failed " + ex.Message;
                }
                
            }
            else
            {
                lblTextMessage.Text = "Please Select a contact to text";
            }
        }

        private void btnMakeCall_Click(object sender, EventArgs e)
        {
            Guid CallerID = dmc.GetCallerID();

            if (cmbRecordings.SelectedItem != null)
            {
                KeyValuePair<string, string> kvp = (KeyValuePair<string, string>)cmbRecordings.SelectedItem;

                Guid RecordingID = new Guid(kvp.Key.Trim());

                List<ContactAttributes> lstCA = new List<ContactAttributes>();

                foreach (GridViewRowInfo gRow in RadGrid3.Rows)
                {
                    string bState = "0";

                    if (gRow.Cells[0].Value != null)
                    {
                        bState = gRow.Cells[0].Value.ToString();
                    }

                    if (bState == "True")
                    {
                        string FirstName = gRow.Cells[1].Value.ToString();
                        string LastName = gRow.Cells[2].Value.ToString();
                        string Phone = gRow.Cells[3].Value.ToString();
                        string Email = gRow.Cells[4].Value.ToString();
                        ContactAttributes CA = new ContactAttributes(Phone, FirstName, LastName, Email);
                        lstCA.Add(CA);
                    }
                }
                if (lstCA.Count > 0)
                {
                    try
                    {
                        dmc.MakeCall(txtCallBroadcastName.Text, CallerID, RecordingID, lstCA);
                        lblCalls.Text = "Call made successfully";
                    }catch (Exception ex)
                    {
                        lblCalls.Text = "Call failed " + ex.Message;
                    }
                }
                else
                {
                    lblCalls.Text = "Please Select a contact to call";
                }
                
            }
            else
            {
                lblCalls.Text = "Please select a recording";
            }
        }

        private void ReloadAll()
        {
            ReloadContactPage();
            ReloadTextMessagePage();
            ReloadCallPage();
        }

        private void ReloadContactPage()
        {
            DataTable dt = dmc.GetContacts();
            RadGrid1.DataSource = dt;

            Dictionary<string, string> Groups = dmc.GetGroups();
            cmbGroups.Items.Clear();
            foreach (KeyValuePair<string, string> result in Groups)
            {
                cmbGroups.Items.Add(result);
                cmbGroups.DisplayMember = "Value";
                cmbGroups.ValueMember = "Key";
            }

            cmbDeleteContacts.Items.Clear();
            cmbDeleteContacts.SelectedItem = null;
            cmbDeleteContacts.Text = "";
            cmbDeleteContacts.Refresh();
            foreach (DataRow item in dt.Rows)
            {
                string strName = item["FirstName"].ToString().Trim() + " " + item["LastName"].ToString().Trim() + " " + item["Phone"].ToString().Trim();

                string strPhone = item["Guid"].ToString().Trim();

                KeyValuePair<string, string> kvp = new KeyValuePair<string, string>(strPhone, strName);

                cmbDeleteContacts.Items.Add(kvp);
                cmbDeleteContacts.DisplayMember = "Value";
                cmbDeleteContacts.ValueMember = "Key";
            }

        }

        private void ReloadTextMessagePage()
        {
            DataTable dt = dmc.GetContacts();
            RadGrid2.DataSource = dt;
            RadGrid2.Columns[0].Width = 75;
        }

        private void ReloadCallPage()
        {
            DataTable dt = dmc.GetContacts();
            RadGrid3.DataSource = dt;
            RadGrid3.Columns[0].Width = 75;

            Dictionary<string, string> Recordings = dmc.GetRecordings();
            cmbRecordings.Items.Clear();
            cmbRecordings.Text = "";
            foreach (KeyValuePair<string, string> result in Recordings)
            {
                cmbRecordings.Items.Add(result);
                cmbRecordings.DisplayMember = "Value";
                cmbRecordings.ValueMember = "Key";
            }
            cmbRecordings.SelectedIndex = 1;
        }
    }
}
